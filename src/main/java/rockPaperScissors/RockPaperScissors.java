package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        System.out.println("Let's play round " + roundCounter);

        int a = 1;
        while (a == 1){
        // Human choice
        String userchoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
        if (rpsChoices.contains(userchoice) == false){
            System.out.printf("I do not understand %s. Could you try again?\n", userchoice);
            continue;
        }
        //Computer choice
        String randomchoice = rpsChoices.get(new Random().nextInt(rpsChoices.size()));
        // output. Resultat skal også være i denne linjen under.
        if (userchoice.equals(randomchoice)) {
            System.out.printf("Human chose %s, computer chose %s. It's a tie!\n", userchoice, randomchoice);
            System.out.printf("Score: human %s, computer %s\n",humanScore, computerScore);
        }   
        else if (userchoice.equals("rock")){
                if (randomchoice == "scissors"){
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", userchoice, randomchoice);
                humanScore +=1;
                System.out.printf("Score: human %s, computer %s\n",humanScore, computerScore);
                }
                else{
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", userchoice, randomchoice);
                computerScore +=1;
                System.out.printf("Score: human %s, computer %s\n",humanScore, computerScore);
                }
        }
        else if (userchoice.equals("scissors")){
                if (randomchoice == "paper"){
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", userchoice, randomchoice);
                humanScore +=1;
                System.out.printf("Score: human %s, computer %s\n",humanScore, computerScore);
                }
                else{
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", userchoice, randomchoice);
                computerScore +=1;
                System.out.printf("Score: human %s, computer %s\n",humanScore, computerScore);  
                }
        }
        else if (userchoice.equals("paper")){
                if (randomchoice == "rock"){
                System.out.printf("Human chose %s, computer chose %s. Human wins!\n", userchoice, randomchoice);
                humanScore +=1;
                System.out.printf("Score: human %s, computer %s\n",humanScore, computerScore);
                }
                else{
                System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", userchoice, randomchoice);
                computerScore +=1;
                System.out.printf("Score: human %s, computer %s\n",humanScore, computerScore);
                }
        }
        while (a == 1){
        String redo = readInput("Do you wish to continue playing? (y/n)?");
        if (redo.equals("n")){
            System.out.println("Bye bye :)");
            a = 0;
        }
        else if (redo.equals("y")){
            roundCounter += 1;
            System.out.println("Let's play round " + roundCounter);
            break;
        }
        else{
            System.out.println("Try again!");
            continue;
        }
        }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
